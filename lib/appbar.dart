import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:admin_360/login_page.dart';
import 'package:flutter/cupertino.dart';

class TopAppBar extends StatefulWidget {
  @override
  _TopAppBarState createState() => _TopAppBarState();
}

class _TopAppBarState extends State<TopAppBar> {
  SharedPreferences sharedPreferences;
  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("api_token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Image.asset('images/logo.png', fit: BoxFit.cover, height: 40.0),
      centerTitle: true,
      backgroundColor: Color(0XFF000000),
      iconTheme: new IconThemeData(color: Color(0XFF97999B)),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            _onBackPressed();
          },
          child: Icon(Icons.power_settings_new, color: Color(0XFF97999B)),
        ),
      ],
    );
  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Container(
          padding: EdgeInsets.only(top: 20.0, bottom: 5.0),
          child: Text('ATTENSION!',
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w600,
                fontSize: 16.0,
              )),
        ),
        content: Container(
          padding: EdgeInsets.only(top: 0.0, bottom: 15.0),
          child: Text('Are you sure you want to logout?',
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w300,
                fontSize: 12.0,
              )),
        ),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text("NO",
                style: TextStyle(
                  color: Color(0XFFFF4444),
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0,
                )),
          ),
          new FlatButton(
            onPressed: () {
              sharedPreferences.clear();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()),
                      (Route<dynamic> route) => false);
            },
            child: Text("YES",
                style: TextStyle(
                  color: Color(0XFF000000),
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0,
                )),
          ),
        ],
      ),
    ) ??
        false;
  }
}