import 'package:flutter/material.dart';
import 'package:admin_360/main.dart';
import 'package:admin_360/report.dart';
import 'package:admin_360/ragent.dart';
import 'login_page.dart';

class AppDrawer extends StatelessWidget {
  final Data PData;
  AppDrawer({this.PData});
  String Purl="";
  @override
  Widget build(BuildContext context) {
    //-//print(PData.pUrl);
    final NewPdata = Data(pUrl: PData.pUrl, ApiKey: PData.ApiKey);
    return Drawer(
      child: new ListView(
        children: <Widget>[
          ListItem("Campaigns", "List of all Campaigns", context,
              Icons.campaign, MainPage(data: NewPdata)),
          Divider(),
          ListItem("Realtime Report", "Realtime Agent Stats Report", context,
              Icons.pie_chart, reportPage(data: NewPdata)),
          Divider(),
          ListItem("Remote Agent", "List of all the remote Agents", context,
              Icons.support_agent, ragentPage(data: NewPdata)),
          Divider(),
        ],
      ),
    );
  }
  ListTile ListItem(String Title, SubTitle, Context, IconData icon, RoutePage) {
    return ListTile(
        leading: Icon(icon, color: Color(0XFF333333), size: 28.0),
        title: Align(
          child: new Text(Title,
              style: TextStyle(
                color: Color(0XFF333333),
                fontSize: 18.0,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w600,
              )),
          alignment: Alignment(-1.2, 0),
        ),
        subtitle: Align(
          child: new Text(SubTitle,
              style: TextStyle(
                color: Color(0XFF333333),
                fontSize: 10.0,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w300,
              )),
          alignment: Alignment(-1.2, 0),
        ),
        onTap: () {
          Navigator.push(
            Context,
            MaterialPageRoute(builder: (Context) => RoutePage),
          );
        });
  }
}
