import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class BottomBar extends StatelessWidget {
  double CBalance = 0;
  SharedPreferences sharedPreferences;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: kToolbarHeight,
      child: AppBar(
        leading: Icon(Icons.account_balance_wallet_rounded,
            color: Color(0XFF000000), size: 26.0),
        title: Align(
          child: new Text(CBalance.toString(), //// BALANCE HERE
              style: TextStyle(
                color: Color(0XFF333333),
                fontSize: 16.0,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w600,
              )),
          alignment: Alignment(-1.15, 0),
        ),
        backgroundColor: Color(0XFFFED300),
      ),
    );
  }

  GetBalance(GBurl) async {
    sharedPreferences = await SharedPreferences.getInstance();
    String FinalGBl = "http://" +
        GBurl +
        "/vicidialapp_api/getBalance.php?api_token=" +
        sharedPreferences.getString("api_token");
    var response = await http.get(FinalGBl);
    var JBData = json.decode(response.body);
    if (response.statusCode == 200) {
      if (JBData["balance"] == null) {
        // setState(() {
        //   CBalance = 0;
        // });
      } else {
        // setState(() {
        //   CBalance = JBData["balance"];
        // });
      }
    } else {
      print(response.body);
    }
  }

}
