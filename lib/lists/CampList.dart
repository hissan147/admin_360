import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:admin_360/login_page.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import '../main.dart';

List<Clist> clistFromJson(String str) => List<Clist>.from(json.decode(str).map((x) => Clist.fromJson(x)));
class Clist {
  String campaignId;
  String name;
  int status;
  int dial_level;
  Clist({this.name, this.campaignId, this.dial_level, this.status});
  factory Clist.fromJson(Map<String, dynamic> json) => Clist(name: json["name"], campaignId: json["campaign_id"], dial_level: json["dial_level"], status: json["status"]);
}

class Services {
  static Future<List<Clist>> getClist(url) async {
    try {
      final response = await http.post(url);
      //-//print(url);
      if (response.statusCode == 200) {
        final List<Clist> clist = clistFromJson(response.body);
        return clist;
      } else {
        return List<Clist>();
      }
    } catch (e) {
      return List<Clist>();
    }
  }
}
class CampStatsData {
  String pUrl;
  String ApiKey;
  String campaignId;
  String name;
  int dial_level;
  CampStatsData({this.pUrl, this.ApiKey, this.campaignId,this.name,this.dial_level});
}
class CampList extends StatefulWidget  {
  Data pData;
  CampList({this.pData});
  @override
  _CampListState createState() => new _CampListState(NewData: pData);
}

class MySharedPreferences {
  MySharedPreferences._privateConstructor();
  static final MySharedPreferences instance =
  MySharedPreferences._privateConstructor();
  Future<String> getBooleanValue(String key) async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    return myPrefs.getString(key) ?? "";
  }
}

class _CampListState extends State<CampList> {
  Data NewData;
  _CampListState({this.NewData});
  List<Clist> _Clist = [];
  bool _SStatus = false;
  String campaignId = "";
  String url="";
  String token="";
  String Purl="";
  double CBalance = 0.0;
  String name;
  SharedPreferences sharedPreferences;

  shared() {
    // SharedPreferences.getInstance().then((SharedPreferences sp) {
    //   sharedPreferences = sp;
    //   url = sharedPreferences.getString('url');
    //   token = sharedPreferences.getString('api_token');
    //   // will be null if never previously saved
    // });
  }
  @override
  void initState() {
    super.initState();
    // shared();
    MySharedPreferences.instance
        .getBooleanValue("url")
        .then((value) => setState(() {
      url= value;
    }));
    MySharedPreferences.instance
        .getBooleanValue("api_token")
        .then((value) => setState(() {
      token= value;
      if(token!="" || token!=null)
      {
        Purl = "http://" + url+ "/vicidialapp_api/campaigns_api.php?api_token=" + token;
        Services.getClist(Purl).then((clist) {
          setState(() {
            _Clist = clist;
          });
        });
        GetBalance(url);
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    //-//print("Key: "+NewData.ApiKey);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(bottom: 50.0),
        child: ListView(
          children: <Widget>[
            Card(
              margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
              shadowColor: Color(0X1A000000),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 30.0, vertical: 13.0),
                leading:
                Icon(Icons.campaign, color: Color(0XFF000000), size: 26.0),
                title: Align(
                  child: new Text(
                    'Campaigns',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0,
                    ),
                  ),
                  alignment: Alignment(-1.25, 0),
                ),
              ),
            ),
            DataTable(
              headingRowColor:
              MaterialStateColor.resolveWith((states) => Color(0xFFEFEFEF)),
              headingRowHeight: 40.0,
              dataRowColor:
              MaterialStateColor.resolveWith((states) => Color(0xFFFFFFFF)),
              dataRowHeight: 48.0,
              columnSpacing: 0.0,
              showBottomBorder: true,
              dividerThickness: 0.5,
              columns: <DataColumn>[
                DataColumn(
                    label: Text("Name",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )),
                    numeric: false),
                DataColumn(
                    label: Expanded(
                child: Text("Dial Level",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )))),
                DataColumn(
                    label: Expanded(
    child: Text("Status",
        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )))),
              ],
              rows: _Clist.map(((clist) => DataRow(
                  cells: [
                    DataCell(Text(clist.name,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 13.0,
                        ))),
                    DataCell(
                      TextFormField(
                        initialValue: clist.dial_level.toString(),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w600,
                          fontSize: 13.0,
                        ),
                        onFieldSubmitted: (val) {
                          //-//print('Submitted Freq is: ' + val);
                          //-//print(clist.campaignId);
                          updateFrequency(clist.campaignId, val);
                        },
                      ),
                    ),
                    DataCell(
                      Align(
                          alignment: Alignment.center,
                          child: (CupertinoSwitch(
                            activeColor: Color(0xFF2ECC71), //(clist.id.toString()==id ?_SStatus:(clist.status == 1 ? true : false))
                            trackColor: Color(0xFFEFEFEF),
                            value: _SStatus == null ? (clist.status == 1 ? true : false) : (clist.campaignId.toString()==campaignId ?_SStatus:(clist.status == 1 ? true : false)),//(clist.status == 1 ? true : false)
                            onChanged: (bool newValue) {
                              this.setState(() {
                                _SStatus = newValue;
                                campaignId=clist.campaignId.toString();
                              });
                              updateStatus(_SStatus == true ? 1 : 4, clist.campaignId ,Purl,_Clist);
                            },
                          ))),
                    )
                  ])),).toList(),
            )
          ],
        ),
      ),
      bottomSheet: Container(
        height: kToolbarHeight,
        child: AppBar(
          leading: Icon(Icons.monetization_on,
              color: Color(0XFF333333), size: 26.0),
          title: Align(
            child: new Text(CBalance.toString(), //// BALANCE HERE
                style: TextStyle(
                  color: Color(0XFF333333),
                  fontSize: 16.0,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w600,
                )),
            alignment: Alignment(-1.20, 0),
          ),
          backgroundColor: Color(0XFFFBE039),
        ),
      ),
    );
  }
  GetBalance(GBurl) async {
    sharedPreferences = await SharedPreferences.getInstance();
    String FinalGBl = "http://" + GBurl + "/vicidialapp_api/getBalance.php?api_token=" + sharedPreferences.getString("api_token");
    var response = await http.get(FinalGBl);
    var JBData = json.decode(response.body);
    if (response.statusCode == 200) {
      if (JBData["balance"] == null) {
        setState(() { CBalance = 0; });
      } else {
        setState(() { CBalance = double.parse((JBData["balance"]).toStringAsFixed(2)); });
      }
    } else {
      print(response.body);
    }
  }

  updateStatus(int DBState,String CampID,Purl,  data) async {
    /* data.forEach((element) {
      if(element.id==CampID){}
    });*/
    String cc=CampID;
    String SS=CampID.toString();
    String SStaS=DBState.toString();
    String MSG = "Connection Error!";
    String StatusChangeurl = "http://" + url +"/vicidialapp_api/update_campaign_status.php?cid=" +SS + "&status=" + SStaS +"&api_token=" +token;
    var response = await http.post(StatusChangeurl);
    if (response.statusCode == 200) {
      //-//print(DBState);
      MSG = DBState == 1 ? "Campaign Started" : "Campaign Stopped";
      _Clist.forEach((element) {
        if(element.campaignId==cc)
        {
          element.status=DBState;
        }
      });
      if(DBState=="1") {}
      _Ftoast(MSG, 1);
      /*  Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => CampList()), (Route<dynamic> route) => false);*/
    } else {
      print(response.body);
      _Ftoast(MSG, 1);
    }
  }
  updateFrequency(clistID, clistFreq) async {
    String FreqChangeurl = "http://" +
        NewData.pUrl +
        "/vicidialapp_api/update_campaign_diallevel.php?cid="+clistID.toString()+"&diallevel="+clistFreq.toString()+"&api_token=" +
        NewData.ApiKey;
    //-// print(FreqChangeurl);
    var response = await http.post(FreqChangeurl);
    if (response.statusCode == 200) {
      _Ftoast("Frequency is successfuly updated", 1);
    } else {
      print(response.body);
    }
  }
  _Ftoast(MsG, SeC) {
    Fluttertoast.showToast(
      msg: MsG,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.SNACKBAR,
      timeInSecForIosWeb: SeC,
      backgroundColor: Colors.black,
      textColor: Colors.white,
    );
  }
}