import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:admin_360/login_page.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import '../main.dart';

List<RAlist> ralistFromJson(String str) => List<RAlist>.from(json.decode(str).map((x) => RAlist.fromJson(x)));
class RAlist {
  String remoteAgentId;
  String remoteAgentName;
  String campaignId;
  String numberOfLines;
  int status;
  RAlist({this.remoteAgentId, this.remoteAgentName, this.campaignId, this.numberOfLines, this.status});
  factory RAlist.fromJson(Map<String, dynamic> json) => RAlist(remoteAgentId: json['remote_agent_id'], campaignId: json["campaign_id"], remoteAgentName: json["remote_agent_name"], numberOfLines: json["number_of_lines"], status: json["status"]);
}

class Services {
  static Future<List<RAlist>> getRAlist(url) async {
    try {
      final response = await http.post(url);
      //-//print(url);
      if (response.statusCode == 200) {
        final List<RAlist> ralist = ralistFromJson(response.body);
        return ralist;
      } else {
        return List<RAlist>();
      }
    } catch (e) {
      return List<RAlist>();
    }
  }
}
class RAStatsData {
  String pUrl;
  String ApiKey;
  String remoteAgentId;
  String remoteAgentName;
  String numberOfLines;
  RAStatsData({this.pUrl, this.ApiKey, this.remoteAgentId,this.remoteAgentName,this.numberOfLines});
}
class RagentList extends StatefulWidget  {
  Data pData;
  RagentList({this.pData});
  @override
  _RagentState createState() => new _RagentState(NewData: pData);
}

class MySharedPreferences {
  MySharedPreferences._privateConstructor();
  static final MySharedPreferences instance =
  MySharedPreferences._privateConstructor();
  Future<String> getBooleanValue(String key) async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    return myPrefs.getString(key) ?? "";
  }
}

class _RagentState extends State<RagentList> {
  Data NewData;
  _RagentState({this.NewData});
  List<RAlist> _RAlist = [];
  bool _SStatus = false;
  String remoteAgentId = "";
  String url="";
  String token="";
  String Purl="";
  double CBalance = 0.0;
  String name;
  SharedPreferences sharedPreferences;
  shared() {
    // SharedPreferences.getInstance().then((SharedPreferences sp) {
    //   sharedPreferences = sp;
    //   url = sharedPreferences.getString('url');
    //   token = sharedPreferences.getString('api_token');
    //   // will be null if never previously saved
    // });
  }
  @override
  void initState() {
    super.initState();
    // shared();
    MySharedPreferences.instance
        .getBooleanValue("url")
        .then((value) => setState(() {
      url= value;
    }));
    MySharedPreferences.instance
        .getBooleanValue("api_token")
        .then((value) => setState(() {
      token= value;
      if(token!="" || token!=null)
      {
        Purl = "http://" + url+ "/vicidialapp_api/remote_agents_api.php?api_token=" + token;
        Services.getRAlist(Purl).then((ralist) {
          setState(() {
            _RAlist = ralist;
          });
        });
        GetBalance(url);
        //-//print(Purl);
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    //-//print("Key: "+NewData.ApiKey);
    return Scaffold(
      body:Container(
        child: ListView(
          children: <Widget>[
        Card(
          margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
          shadowColor: Color(0X1A000000),
          child: ListTile(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 30.0, vertical: 13.0),
            leading:
            Icon(Icons.support_agent, color: Color(0XFF000000), size: 26.0),
            title: Align(
              child: new Text(
                'Remote Agents',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                ),
              ),
              alignment: Alignment(-1.25, 0),
            ),
          ),
        ),
            DataTable(
              headingRowColor:
              MaterialStateColor.resolveWith((states) => Color(0xFFEFEFEF)),
              headingRowHeight: 40.0,
              dataRowColor:
              MaterialStateColor.resolveWith((states) => Color(0xFFFFFFFF)),
              dataRowHeight: 48.0,
              columnSpacing: 0.0,
              showBottomBorder: true,
              dividerThickness: 0.5,
              columns: <DataColumn>[
                DataColumn(
                    label:  Text("Name",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )),
                    numeric: false),
                DataColumn(
                    label:  Text("Campaigns",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w300,
                              fontSize: 15.0,
                            ))),
                DataColumn(
                    label: Expanded(
                child: Text("Lines",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )))),
                DataColumn(
                    label: Expanded(
                      child: Text("Status",
                          textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )))
                    ),
              ],
              rows: _RAlist.map(((ralist) => DataRow(
                  cells: [
                    DataCell(Text(
                        ralist.remoteAgentName,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 13.0,
                        ))),
                    DataCell(Text(
                        ralist.campaignId,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 13.0,
                        ))
                    ),
                    DataCell(
                      TextFormField(
                        maxLength: 3,
                        buildCounter: (BuildContext context, { int currentLength, int maxLength, bool isFocused }) => null,
                        initialValue: ralist.numberOfLines.toString(),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w600,
                          fontSize: 13.0,
                        ),
                        onFieldSubmitted: (val) {
                          updateLines(ralist.remoteAgentId, val);
                        },
                      ),
                    ),
                    DataCell(
                      Align(
                          alignment: Alignment.center,
                          child: (
                              CupertinoSwitch(
                            activeColor: Color(0xFF2ECC71),              //(ralist.id.toString()==id ?_SStatus:(ralist.status == 1 ? true : false))
                            trackColor: Color(0xFFEFEFEF),
                            value: _SStatus == null ? (ralist.status == 1 ? true : false) : (ralist.remoteAgentId.toString()==remoteAgentId ?_SStatus:(ralist.status == 1 ? true : false)),//(ralist.status == 1 ? true : false)
                            onChanged: (bool newValue) {
                              this.setState(() {
                                _SStatus = newValue;
                                remoteAgentId=ralist.remoteAgentId.toString();
                              });
                              updateStatus(_SStatus == true ? 1 : 4, ralist.remoteAgentId ,Purl,_RAlist);
                            },
                          ))),
                    )
                  ])),).toList(),
            )
          ],
        ),
      ),
      bottomSheet: Container(
        height: kToolbarHeight,
        child: AppBar(
          leading: Icon(Icons.monetization_on,
              color: Color(0XFF333333), size: 26.0),
          title: Align(
            child: new Text(CBalance.toString(), //// BALANCE HERE
                style: TextStyle(
                  color: Color(0XFF333333),
                  fontSize: 16.0,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w600,
                )),
            alignment: Alignment(-1.20, 0),
          ),
          backgroundColor: Color(0XFFFBE039),
        ),
      ),
    );
  }
  GetBalance(GBurl) async {
    sharedPreferences = await SharedPreferences.getInstance();
    String FinalGBl = "http://" + GBurl + "/vicidialapp_api/getBalance.php?api_token=" + sharedPreferences.getString("api_token");
    var response = await http.get(FinalGBl);
    var JBData = json.decode(response.body);
    if (response.statusCode == 200) {
      if (JBData["balance"] == null) {
        setState(() { CBalance = 0; });
      } else {
        setState(() { CBalance = double.parse((JBData["balance"]).toStringAsFixed(2)); });
      }
    } else {
      print(response.body);
    }
  }
  updateStatus(int DBState,String CampID,Purl,  data) async {
    String cc=CampID;
    String SS=CampID.toString();
    String SStaS=DBState.toString();
    String MSG = "Connection Error!";
    String StatusChangeurl = "http://" + url +"/vicidialapp_api/update_agent_status.php?rgid=" +SS + "&status=" + SStaS +"&api_token=" +token;
    var response = await http.post(StatusChangeurl);
    if (response.statusCode == 200) {
      //-//print(DBState);
      MSG = DBState == 1 ? "Agent Status Changed to Active" : "Agent Status Changed to InActive";
      _RAlist.forEach((element) {
        if(element.remoteAgentId==cc) {
          ////////////////////////////element.status=DBState;
        }
      });
      if(DBState=="1") {}
      _Ftoast(MSG, 1);
    } else {
      print(response.body);
      _Ftoast(MSG, 1);
    }
  }
  updateLines(ralistID, ralistlines) async {
    ralistlines = int.parse(ralistlines);
    if(ralistlines >= 250) {
      ralistlines = 250;
    }
    String LinesChangeurl = "http://" + NewData.pUrl + "/vicidialapp_api/update_agent_lines.php?rgid="+ralistID.toString()+"&nolines="+ralistlines.toString()+"&api_token=" + NewData.ApiKey;
    print(LinesChangeurl);
    var response = await http.post(LinesChangeurl);
    if (response.statusCode == 200) {
      _Ftoast("Line's field is successfuly updated", 1);
    } else {
      print(response.body);
    }
  }
  _Ftoast(MsG, SeC) {
    Fluttertoast.showToast(
      msg: MsG,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.SNACKBAR,
      timeInSecForIosWeb: SeC,
      backgroundColor: Colors.black,
      textColor: Colors.white,
    );
  }
}