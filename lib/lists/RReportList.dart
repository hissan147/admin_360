import 'dart:convert';
import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:admin_360/login_page.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

List<RReportlist> rReportlistFromJson(String str) => List<RReportlist>.from(json.decode(str).map((x) => RReportlist.fromJson(x)));
class RReportlist {
  String liveAgentId;
  String liveAgentUserId;
  String campaignId;
  dynamic liveAgentName;
  String agentLastUpdate;
  String statusBgColor;
  String lastUpdateIntervalTime;
  String status;
  RReportlist({
    this.liveAgentId,
    this.liveAgentUserId,
    this.campaignId,
    this.liveAgentName,
    this.agentLastUpdate,
    this.statusBgColor,
    this.lastUpdateIntervalTime,
    this.status,
  });
  factory RReportlist.fromJson(Map<String, dynamic> json) => RReportlist(
    liveAgentId: json["live_agent_id"],
    liveAgentUserId: json["live_agent_user_id"],
    campaignId: json["campaign_id"],
    liveAgentName: json["live_agent_name"],
    agentLastUpdate: json["agent_last_update"],
    lastUpdateIntervalTime: json["last_update_interval_time"],
    statusBgColor: json["status_bg_color"] == null ? "0xFFDEE2E6" : json["status_bg_color"],
    status: json["status"],
  );
}

class Services {
  static Future<List<RReportlist>> getRReportlist(url) async {
    try {
      final response = await http.post(url);
      if (response.statusCode == 200) {
        final List<RReportlist> reportlist = rReportlistFromJson(response.body);
        return reportlist;
      } else {
        return List<RReportlist>();
      }
    } catch (e) {
      return List<RReportlist>();
    }
  }
}
class RreportList extends StatefulWidget  {
  Data pData;
  RreportList({this.pData});
  @override
  _RreportListState createState() => new _RreportListState(NewData: pData);
}

class MySharedPreferences {
  MySharedPreferences._privateConstructor();
  static final MySharedPreferences instance =
  MySharedPreferences._privateConstructor();
  Future<String> getBooleanValue(String key) async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    return myPrefs.getString(key) ?? "";
  }
}

class _RreportListState extends State<RreportList> {
  Data NewData;
  _RreportListState({this.NewData});
  List<RReportlist> _rreportlist = [];
  String remoteAgentId = "";
  String url="";
  String token="";
  String Purl="";
  double CBalance = 0.0;
  String name;
  SharedPreferences sharedPreferences;
  Timer timer;
  int counter = 20;
  Timer Newtimer;

  shared() {
    // SharedPreferences.getInstance().then((SharedPreferences sp) {
    //   sharedPreferences = sp;
    //   url = sharedPreferences.getString('url');
    //   token = sharedPreferences.getString('api_token');
    //   // will be null if never previously saved
    // });
  }
  @override
  void initState() {
    super.initState();
    Newtimer = Timer.periodic(Duration(seconds: 1), (Timer ct) => minusValue());
    timer = Timer.periodic(Duration(seconds: 20), (Timer t) => addValue());
    // shared();
    MySharedPreferences.instance
        .getBooleanValue("url")
        .then((value) => setState(() {
      url= value;
    }));
    MySharedPreferences.instance
        .getBooleanValue("api_token")
        .then((value) => setState(() {
      token= value;
      if(token!="" || token!=null)
      {
        Purl = "http://" + url+ "/vicidialapp_api/real_time_report.php?api_token=" + token;
        Services.getRReportlist(Purl).then((reportlist) {
          setState(() {
            _rreportlist = reportlist;
          });
        });
        GetBalance(url);
        print(Purl);
      }
    }));
  }

  void minusValue(){
    setState(() {
      counter--;
      counter == 0 ? counter = 20 : counter;
    });
  }

  void addValue() {
    setState(() {
      Purl = "http://" + url+ "/vicidialapp_api/real_time_report.php?api_token=" + token;
      Services.getRReportlist(Purl).then((reportlist) {
        setState(() {
          _rreportlist = reportlist;
          counter = 20;
        });
      });
      //print(Purl);
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    Newtimer?.cancel();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    //-//print("Key: "+NewData.ApiKey);
    return Scaffold(
      body:Container(
        child: ListView(
          children: <Widget>[
        Card(
          margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
          shadowColor: Color(0X1A000000),
          child: ListTile(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 30.0, vertical: 13.0),
            leading:
            Icon(Icons.pie_chart, color: Color(0XFF000000), size: 26.0),
            title: Align(
              child: new Text(
                'Realtime Report',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                ),
              ),
              alignment: Alignment(-1.25, 0),
            ),
            subtitle: Align(
              child: Text("Auto Refresh In: "+counter.toString()),
              alignment: Alignment(-1.15, 0),
            ),
          ),
        ),
      DataTable(
              headingRowColor:
              MaterialStateColor.resolveWith((states) => Color(0xFFEFEFEF)),
              headingRowHeight: 40.0,
              dataRowColor:
              MaterialStateColor.resolveWith((states) => Color(0xFFFFFFFF)),
              dataRowHeight: 30.0,
              columnSpacing: 0.0,
              showBottomBorder: true,
              dividerThickness: 0.5,
              columns: <DataColumn>[
                DataColumn(
                    label: Text("User",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )),
                    numeric: false),
                DataColumn(
                    label: Text("Campaign",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        ))),
                DataColumn(
                    label: Text("Time",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        ))),
                DataColumn(
                    label: Expanded(
                child: Text("Status",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 15.0,
                        )))
                ),
              ],
              rows: _rreportlist.map(((reportlist) => DataRow(
                  cells: [
                    DataCell(Text(reportlist.liveAgentName == null ? "No Name" : reportlist.liveAgentName,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 13.0,
                        ))),
                    DataCell(Text(reportlist.campaignId,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w300,
                          fontSize: 13.0,
                        ))),
                    DataCell(
                        Text(reportlist.agentLastUpdate.toString(),
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w300,
                              fontSize: 13.0,
                            ))
                    ),
                    DataCell(
                        Container(
                        color: Color(int.parse(reportlist.statusBgColor)),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child:
                                Align(
                                    alignment: Alignment.center,
                                    child:
                                    Text(reportlist.status,
                                        style: TextStyle(
                                          color: reportlist.statusBgColor == "0XFFFED300" ? Colors.black : Colors.white,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w600,
                                          fontSize: 14.0,
                                        )
                                    )
                                )
                            )
                          ],
                        ),
                      ),
                    )
                  ])),).toList(),
            )
          ],
        ),
      ),
      bottomSheet: Container(
        height: kToolbarHeight,
        child: AppBar(
          leading: Icon(Icons.monetization_on,
              color: Color(0XFF333333), size: 26.0),
          title: Align(
            child: new Text(CBalance.toString(), //// BALANCE HERE
                style: TextStyle(
                  color: Color(0XFF333333),
                  fontSize: 16.0,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w600,
                )),
            alignment: Alignment(-1.20, 0),
          ),
          backgroundColor: Color(0XFFFBE039),
        ),
      ),
    );
  }
  GetBalance(GBurl) async {
    sharedPreferences = await SharedPreferences.getInstance();
    String FinalGBl = "http://" + GBurl + "/vicidialapp_api/getBalance.php?api_token=" + sharedPreferences.getString("api_token");
    var response = await http.get(FinalGBl);
    var JBData = json.decode(response.body);
    if (response.statusCode == 200) {
      if (JBData["balance"] == null) {
        setState(() { CBalance = 0; });
      } else {
        setState(() { CBalance = double.parse((JBData["balance"]).toStringAsFixed(2)); });
      }
    } else {
      print(response.body);
    }
  }
}