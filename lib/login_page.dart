import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:admin_360/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Data {
  String pUrl;
  String ApiKey;
  Data({this.pUrl, this.ApiKey});
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
        .copyWith(statusBarColor: Colors.transparent));
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/bg.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : ListView(children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    headerSection(),
                    textSection(),
                    buttonSection(),
                  ],
                ),
              ),
            ])));
  }

  signIn(String domain, email, password) async {
    Map data = {"email": email, "password": password};
    var jsonData = null;
    var url = "http://" + domainController.text + "/vicidialapp_api/login_api.php?user=" + emailController.text + "&pass=" + passwordController.text;
    setState(() {
      _isLoading = false;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      var response = await http.post(url, body: data);
      jsonData = json.decode(response.body);
      if (jsonData["is_active"] == true) {
        final data =
        Data(pUrl: domainController.text, ApiKey: jsonData["api_token"]);
        setState(() {
          _isLoading = false;
          sharedPreferences.setString("api_token", jsonData["api_token"]);
          sharedPreferences.setString("url", domainController.text);
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => MainPage(data: data)),
                  (Route<dynamic> route) => false);
        });
      } else {
        _Ftoast(
            "Incorrect Username/Password or You are inactive in our system, Kindly contact with admnistrator.",
            2,
            Colors.redAccent,
            Colors.white);
      }
    } on SocketException catch (_) {
      _Ftoast("Your Domain url is wrong!", 2, Colors.redAccent, Colors.white);
    }
  }

  Container buttonSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      margin: EdgeInsets.symmetric(horizontal: 50.0, vertical: 5.0),
      color: Color(0XFFFBE039),
      child: MaterialButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            setState(() {
              _isLoading = true;
            });
          }
          signIn(domainController.text, emailController.text,
              passwordController.text);
        },
        child: Text("LOGIN",
            style: TextStyle(
              color: Colors.black,
              fontFamily: "Poppins",
            )),
      ),
    );
  }
  TextEditingController domainController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  Container textSection() {
    return Container(
      margin: EdgeInsets.only(top: 30.0, right: 50.0, left: 50.0),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
            margin: EdgeInsets.only(bottom: 20.0),
            color: Color(0x1AFFFFFF),
            child: Column(
              children: [
                txtDomain(
                    "Domain", Icons.link, "Domain is required", "Poppins"),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
            margin: EdgeInsets.only(bottom: 20.0),
            color: Color(0x1AFFFFFF),
            child: Column(
              children: [
                txtEmail(
                    "Username", Icons.person, "Missing Username", "Poppins"),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
            margin: EdgeInsets.only(bottom: 20.0),
            color: Color(0x1AFFFFFF),
            child: Column(
              children: [
                txtPassword("Password", Icons.more_horiz, "Missing Password",
                    "Poppins"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  TextFormField txtDomain(
      String Ttitle, IconData Ticon, ValidText, FontFamily) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return ValidText;
        }
        return null;
      },
      controller: domainController,
      style: TextStyle(color: Colors.grey.shade400),
      decoration: InputDecoration(
          border: InputBorder.none,
          errorStyle: TextStyle(
            fontSize: 12.0,
            color: Color(0XFFFED300),
            fontFamily: FontFamily,
            fontWeight: FontWeight.w600,
          ),
          hintText: Ttitle,
          hintStyle: TextStyle(
            color: Colors.grey.shade600,
            fontFamily: FontFamily,
          ),
          icon: Icon(Ticon, color: Colors.grey.shade600)),
    );
  }

  TextFormField txtEmail(String Ttitle, IconData Ticon, ValidText, FontFamily) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return ValidText;
        }
        return null;
      },
      controller: emailController,
      style: TextStyle(color: Colors.grey.shade400),
      decoration: InputDecoration(
          border: InputBorder.none,
          errorStyle: TextStyle(
            fontSize: 12.0,
            color: Color(0XFFFED300),
            fontFamily: FontFamily,
            fontWeight: FontWeight.w600,
          ),
          hintText: Ttitle,
          hintStyle: TextStyle(
            color: Colors.grey.shade600,
            fontFamily: FontFamily,
          ),
          icon: Icon(Ticon, color: Colors.grey.shade600)),
    );
  }

  TextFormField txtPassword(
      String Ttitle, IconData Ticon, ValidText, FontFamily) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return ValidText;
        }
        return null;
      },
      controller: passwordController,
      obscureText: true,
      style: TextStyle(color: Colors.grey.shade400),
      decoration: InputDecoration(
          border: InputBorder.none,
          errorStyle: TextStyle(
            fontSize: 12.0,
            color: Color(0XFFFED300),
            fontFamily: FontFamily,
            fontWeight: FontWeight.w600,
          ),
          hintText: Ttitle,
          hintStyle: TextStyle(
            color: Colors.grey.shade600,
            fontFamily: FontFamily,
          ),
          icon: Icon(Ticon, color: Colors.grey.shade600)),
    );
  }

  Container headerSection() {
    return Container(
      padding: EdgeInsets.only(top: 120.0, bottom: 25.0),
      child: SizedBox(
        height: 80.0,
        child: Image.asset(
          "images/logo.png",
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  _Ftoast(MsG, SeC, CColor, TxtColor) {
    Fluttertoast.showToast(
      msg: MsG,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.SNACKBAR,
      timeInSecForIosWeb: SeC,
      backgroundColor: CColor,
      textColor: TxtColor,
    );
  }
}
