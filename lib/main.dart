import 'package:admin_360/appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'appdrawer.dart';
import 'lists/CampList.dart';
import 'login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainPage(),
      title: 'Admin',
      theme: ThemeData(accentColor: Colors.white70, primaryColor: Color(0XFF000000)),
    );
  }
}

class MainPage extends StatefulWidget {
  Data data;
  MainPage({this.data});
  @override
  _MainPageState createState() => new _MainPageState(NewData: data);
}

class _MainPageState extends State<MainPage> {
  Data NewData;
  _MainPageState({this.NewData});
  double CBalance = 0;
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
    if(NewData != null){
      GetBalance(NewData.pUrl);
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent));
    return
    WillPopScope(
        onWillPop: () {
      _onBackPressed();
    },
    child: Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0), // here the desired height
          child: TopAppBar(),
      ),
      drawer: Drawer(child: AppDrawer(PData: NewData)),
      body: CampList(pData: NewData),
      // bottomSheet: Container(
      //   height: kToolbarHeight,
      //   child: BottomBar(),
      // ),
    ));
  }

  // HELPING FUNCTIONS
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("api_token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }
  }
  GetBalance(GBurl) async {
    sharedPreferences = await SharedPreferences.getInstance();
    String FinalGBl = "http://" + GBurl + "/vicidialapp_api/getBalance.php?api_token=" + sharedPreferences.getString("api_token");
    var response = await http.get(FinalGBl);
    var JBData = json.decode(response.body);
    if (response.statusCode == 200) {
      if (JBData["balance"] == null) {
        setState(() { CBalance = 0; });
      } else {
        setState(() { CBalance = JBData["balance"]; });
      }
    } else {
      print(response.body);
    }

  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Container(
          padding: EdgeInsets.only(top: 20.0, bottom: 5.0),
          child: Text('ATTENSION!',
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w600,
                fontSize: 16.0,
              )),
        ),
        content: Container(
          padding: EdgeInsets.only(top: 0.0, bottom: 15.0),
          child: Text('Are you sure you want to logout?',
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w300,
                fontSize: 12.0,
              )),
        ),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text("NO",
                style: TextStyle(
                  color: Color(0XFFFF4444),
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0,
                )),
          ),
          new FlatButton(
            onPressed: () {
              sharedPreferences.clear();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()),
                      (Route<dynamic> route) => false);
            },
            child: Text("YES",
                style: TextStyle(
                  color: Color(0XFF000000),
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0,
                )),
          ),
        ],
      ),
    ) ??
        false;
  }
}
