import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:admin_360/appdrawer.dart';
import 'package:admin_360/lists/RAgentList.dart';
import 'appbar.dart';
import 'login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'main.dart';

class ragentPage extends StatefulWidget {
  Data data;
  ragentPage({this.data});
  @override
  _ragentState createState() => new _ragentState(NewData: data);
}

class _ragentState extends State<ragentPage> {
  Data NewData;
  _ragentState({this.NewData});
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("api_token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (Context) => MainPage(data: NewData)),
          );
        },
        child: Scaffold(
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(60.0), // here the desired height
              child: TopAppBar()
          ),
          drawer: Drawer(child: AppDrawer(PData: NewData)),
          body: RagentList(pData: NewData),
        ));
  }
}
