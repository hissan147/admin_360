import 'package:flutter/material.dart';
import 'package:admin_360/appdrawer.dart';
import 'package:admin_360/appbar.dart';
import 'lists/RReportList.dart';
import 'package:admin_360/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'login_page.dart';

class reportPage extends StatefulWidget {
  Data data;
  reportPage({this.data});
  @override
  _rreportState createState() => new _rreportState(NewData: data);
}

class _rreportState extends State<reportPage> {
  Data NewData;
  _rreportState({this.NewData});
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("api_token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return
      WillPopScope(
        onWillPop: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (Context) => MainPage(data: NewData)),
          );
    },
    child:  Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0), // here the desired height
          child: TopAppBar()),
      drawer: Drawer(child: AppDrawer(PData: NewData)),
      body: RreportList(),
    ));
  }
}
